#set up enviroment
install.packages("magick")
install.packages("rsvg")
library(magick)
library(rsvg)
str(magick::magick_config())
path <- "C:\\ML\\AOI\\Test_Image\\"

#create filenames list
image_file <- list.files(path = path, pattern = "*.png")
picture_list<-image_join()

#loading image
for (file in image_file) {
  filename = strsplit(file, "[.]")[[1]][1] #split file names with "."
  picture_list <- image_join(picture_list,assign(filename,image_read(paste(path, file, sep = ""))))
  #assign filename with image
}

#encoding region
left <- "171x512+1+0"
mid <- "170x512+172+0"
right <- "171x512+341+0"

#encoding region
top <- "512x171+0+1"
midd <- "512x170+0+172"
bottom <- "512x171+0+341"

setwd("C:\\ML\\AOI\\top_test")
#cutting image and writting out
for (file in seq(1,length(image_file),1)) {
  filename = strsplit(image_file[file], "[.]")[[1]][1] #split file names with "."
  cutted_filename = paste(filename,"top")
  image_write(assign(cutted_filename,image_crop(picture_list[file],top)),path = cutted_filename, format = "png")
}

setwd("C:\\ML\\AOI\\bottom_test")
#cutting image and writting out
for (file in seq(1,length(image_file),1)) {
  filename = strsplit(image_file[file], "[.]")[[1]][1] #split file names with "."
  cutted_filename = paste(filename,"bottom")
  image_write(assign(cutted_filename,image_crop(picture_list[file],bottom)),path = cutted_filename, format = "png")
}

setwd("C:\\ML\\AOI\\mid_horizontal_test")
#cutting image and writting out
for (file in seq(1,length(image_file),1)) {
  filename = strsplit(image_file[file], "[.]")[[1]][1] #split file names with "."
  cutted_filename = paste(filename,"midd")
  image_write(assign(cutted_filename,image_crop(picture_list[file],midd)),path = cutted_filename, format = "png")
}


#gaussian
test<-image_blur(image,5,3)
#median
test2<-image_reducenoise(image,radius = 5)

setwd("C:\\ML\\AOI\\blur_test")
#cutting image and writting out
for (file in seq(1,length(image_file),1)) {
  filename = strsplit(image_file[file], "[.]")[[1]][1] #split file names with "."
  cutted_filename = paste(filename,"blur")
  image_write(assign(cutted_filename,image_blur(picture_list[file],5,3)),path = cutted_filename, format = "png")
}
