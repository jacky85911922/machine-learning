data<-read.csv("C:\\ML\\AOI\\Haralick.feature(full).csv")
data<-read.csv("C:\\ML\\AOI\\Haralick.feature(left bottom).csv")
data<-read.csv("C:\\ML\\AOI\\Haralick.feature(left top).csv")
data<-read.csv("C:\\ML\\AOI\\Haralick.feature(right bottom).csv")
data<-read.csv("C:\\ML\\AOI\\Haralick.feature(right top).csv")
data<-read.csv("C:\\ML\\AOI\\df_feature0_128.csv")
data<-read.csv("C:\\ML\\AOI\\df_feature0_256.csv")
library(tidyverse)
library(gridExtra)

#f1_curve_label
f1_curve_label_2<-ggplot(data = data) + 
  geom_smooth(mapping = aes(x = X, y = f1),
              method = "loess", formula = "y ~ x") + 
  geom_point(mapping = aes(x = X, y = f1,color=Label),alpha=0.3)+
  scale_color_distiller(palette = "Spectral")

#f2_curve_label
f2_curve_label_2<-ggplot(data = data) + 
  geom_smooth(mapping = aes(x = X, y = f2),
              method = "loess", formula = "y ~ x") + 
  geom_point(mapping = aes(x = X, y = f2,color=Label),alpha=0.3)+
  scale_color_distiller(palette = "Spectral")

#f3_curve_label
f3_curve_label_2<-ggplot(data = data) + 
  geom_smooth(mapping = aes(x = X, y = f3),
              method = "loess", formula = "y ~ x") + 
  geom_point(mapping = aes(x = X, y = f3,color=Label),alpha=0.3)+
  scale_color_distiller(palette = "Spectral")

#f4_curve_label
f4_curve_label_2<-ggplot(data = data) + 
  geom_smooth(mapping = aes(x = X, y = f4),
              method = "loess", formula = "y ~ x") + 
  geom_point(mapping = aes(x = X, y = f4,color=Label),alpha=0.3)+
  scale_color_distiller(palette = "Spectral")

#f5_curve_label
f5_curve_label_2<-ggplot(data = data) + 
  geom_smooth(mapping = aes(x = X, y = f5),
              method = "loess", formula = "y ~ x") + 
  geom_point(mapping = aes(x = X, y = f5,color=Label),alpha=0.3)+
  scale_color_distiller(palette = "Spectral")

#f6_curve_label
f6_curve_label_2<-ggplot(data = data) + 
  geom_smooth(mapping = aes(x = X, y = f6),
              method = "loess", formula = "y ~ x") + 
  geom_point(mapping = aes(x = X, y = f6,color=Label),alpha=0.3)+
  scale_color_distiller(palette = "Spectral")

#f7_curve_label
f7_curve_label_2<-ggplot(data = data) + 
  geom_smooth(mapping = aes(x = X, y = f7),
              method = "loess", formula = "y ~ x") + 
  geom_point(mapping = aes(x = X, y = f7,color=Label),alpha=0.3)+
  scale_color_distiller(palette = "Spectral")

#f8_curve_label
f8_curve_label_2<-ggplot(data = data) + 
  geom_smooth(mapping = aes(x = X, y = f8),
              method = "loess", formula = "y ~ x") + 
  geom_point(mapping = aes(x = X, y = f8,color=Label),alpha=0.3)+
  scale_color_distiller(palette = "Spectral")


#f9_curve_label
f9_curve_label_2<-ggplot(data = data) + 
  geom_smooth(mapping = aes(x = X, y = f9),
              method = "loess", formula = "y ~ x") + 
  geom_point(mapping = aes(x = X, y = f9,color=Label),alpha=0.3)+
  scale_color_distiller(palette = "Spectral")

#f10_curve_label
f10_curve_label_2<-ggplot(data = data) + 
  geom_smooth(mapping = aes(x = X, y = f10),
              method = "loess", formula = "y ~ x") + 
  geom_point(mapping = aes(x = X, y = f10,color=Label),alpha=0.3)+
  scale_color_distiller(palette = "Spectral")

#f11_curve_label
f11_curve_label_2<-ggplot(data = data) + 
  geom_smooth(mapping = aes(x = X, y = f11),
              method = "loess", formula = "y ~ x") + 
  geom_point(mapping = aes(x = X, y = f11,color=Label),alpha=0.3)+
  scale_color_distiller(palette = "Spectral")

#f12_curve_label
f12_curve_label_2<-ggplot(data = data) + 
  geom_smooth(mapping = aes(x = X, y = f12),
              method = "loess", formula = "y ~ x") + 
  geom_point(mapping = aes(x = X, y = f12,color=Label),alpha=0.3)+
  scale_color_distiller(palette = "Spectral")

#f13_curve_label
f13_curve_label_2<-ggplot(data = data) + 
  geom_smooth(mapping = aes(x = X, y = f13),
              method = "loess", formula = "y ~ x") + 
  geom_point(mapping = aes(x = X, y = f13,color=Label),alpha=0.3)+
  scale_color_distiller(palette = "Spectral")

grid.arrange(f3_curve_label_2,f5_curve_label_2,f8_curve_label_2
             ,f12_curve_label_2, ncol=2)

grid.arrange(f1_curve_label_2,f2_curve_label_2,f4_curve_label_2,f6_curve_label_2
             ,f7_curve_label_2,f9_curve_label_2,f10_curve_label_2
             ,f11_curve_label_2,f13_curve_label_2,ncol=3)
